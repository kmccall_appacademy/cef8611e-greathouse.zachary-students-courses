class Student
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  attr_reader :first_name
  attr_reader :last_name

  def name
    "#{@first_name} #{@last_name}"
  end

  attr_reader :courses

  def enroll(new_course)

    @courses.each do |course|
      if course.conflicts_with?(new_course)
        return raise_error
      end
    end

    unless @courses.include?(new_course)
        self.courses << new_course
    end

    new_course.students << self
  end

  def course_load
    all_courses = {}

    @courses.each do |course|
      if all_courses.include?(course.department)
        all_courses[course.department] += course.credits
      else
        all_courses[course.department] = course.credits
      end
    end

    all_courses
  end
end
